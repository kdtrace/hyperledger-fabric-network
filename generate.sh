#!/bin/sh
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
export PATH=$GOPATH/src/github.com/hyperledger/fabric/build/bin:${PWD}/bin:${PWD}:$PATH
export FABRIC_CFG_PATH=${PWD}

# Environment variable for channel kdtrace
CHANNEL_NAME=kdtrace
CHANNEL_TX=$CHANNEL_NAME-channel.tx

# remove previous crypto material and config transactions
rm -fr ./network-config/*
rm -fr ./crypto-config/*

# generate ctc crypto material
cryptogen generate --config=./crypto-config.yaml
if [ "$?" -ne 0 ]; then
  echo "Failed to generate crypto material..."
  exit 1
fi


if [ ! -d "./network-config" ]; then
  echo "Create network-config directory!"
  mkdir ./network-config
fi

# generate genesis block for orderer
configtxgen -profile KDTraceIGenesis -outputBlock ./network-config/genesis.block
if [ "$?" -ne 0 ]; then
  echo "Failed to generate orderer genesis block..."
  exit 1
fi

# generate channel kdtrace configuration transaction
configtxgen -profile KDTraceChannel -outputCreateChannelTx ./network-config/$CHANNEL_TX -channelID $CHANNEL_NAME
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi

# generate anchor peer transaction for organization Org1 and channel kdtrace
configtxgen -profile KDTraceChannel -outputAnchorPeersUpdate ./network-config/Org1MSPanchors-$CHANNEL_NAME.tx -channelID $CHANNEL_NAME -asOrg Org1
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for Org1..."
  exit 1
fi

# generate anchor peer transaction for organization Org2 and channel kdtrace
configtxgen -profile KDTraceChannel -outputAnchorPeersUpdate ./network-config/Org2MSPanchors-$CHANNEL_NAME.tx -channelID $CHANNEL_NAME -asOrg Org2
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for Org2..."
  exit 1
fi

# generate anchor peer transaction for organization Org3 and channel kdtrace
configtxgen -profile KDTraceChannel -outputAnchorPeersUpdate ./network-config/Org3MSPanchors-$CHANNEL_NAME.tx -channelID $CHANNEL_NAME -asOrg Org3
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for Org3..."
  exit 1
fi
