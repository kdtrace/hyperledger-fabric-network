#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

# Environment variable for channel kdtrace
CHANNEL_NAME=kdtrace
CHANNEL_NAME_BLOCK=$CHANNEL_NAME.block
CHANNEL_TX=$CHANNEL_NAME-channel.tx

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=30
export FABRIC_PROCESS_TIMEOUT=10

CC_SRC_PATH=/opt/gopath/src/github.com/java
CC_RUNTIME_LANGUAGE=java

export CA1_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/org1.kdtrace.vn/ca && ls *_sk)
    
echo "##################################################";
echo "Start Server 1: "${SERVER_1_IP};
echo "##################################################";

docker-compose -f docker-compose-org1.yml up -d
docker ps -a


echo "##################################################";
echo "That tuyet voi, he thong cua ban da hoat dong tot ";
echo "################### Xin cam on ###################";
echo "##################################################";