# Setup

## Step 1: Download Hyperledger Fabric binaries and Docker images

Run ``download.sh`` for download ``bin`` directory and docker images.
### Usage: download.sh [<fabric_version>] [<fabric_ca_version>] [<thirdparty_version>] [-d -b]
> ``-h - this help``
>
> ``-d - bypass docker image download``
>
> ``-b - bypass download of platform-specific binaries``
>
> 
> ``e.g. download.sh 1.4.1 1.4.1 0.4.15``
>
> ``would download docker images and binaries for version 1.4.1 (fabric) 1.4.1 (fabric-ca) 0.4.15 (thirdparty)``

## Step 2:

Run ``generate.sh`` for generate crypto material, genesis block for orderer, channel configuration transaction, anchor peer transaction

## Step 3: Start Network

Run ``start.sh -s``

## Upgrade chaincode

Run ``start.sh -u [<version>] [<policy>]``

> ``[<version>]: Version of chaincode. If current version of chaincode is 1.1, we need set new version is 1.2``
>
> ``[<policy>]: Have two option "AND" and "OR". Default "AND"``
>
> ``      "Or": Require signature from one of the organizations in a transaction.``
>
> ``     "AND": Require signatures from both organizations in a transaction.``