version: '2'

services:
  peer:
    image: hyperledger/fabric-peer:1.4.0
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=info
      - CORE_CHAINCODE_LOGGING_LEVEL=info
      - CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/peer/
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    volumes:
      - /var/run/:/host/var/run/
      - ../network-config:/etc/hyperledger/configtx

  zookeeper:
    image: hyperledger/fabric-zookeeper
    ports:
        - 2181
        - 2888
        - 3888

  kafka:
      image: hyperledger/fabric-kafka
      environment:
          - KAFKA_LOG_RETENTION_MS=-1
          - KAFKA_MESSAGE_MAX_BYTES=103809024
          - KAFKA_REPLICA_FETCH_MAX_BYTES=103809024
          - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false
          - KAFKA_DEFAULT_REPLICATION_FACTOR=${KAFKA_DEFAULT_REPLICATION_FACTOR}
          - KAFKA_MIN_INSYNC_REPLICAS=2
      ports:
          - 9092

  orderer:
      image: hyperledger/fabric-orderer:1.4.0
      environment:
          - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_basic
          - ORDERER_HOME=/var/hyperledger/orderer
          - ORDERER_GENERAL_LOGLEVEL=debug
          - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/msp
          - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
          - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
          - ORDERER_GENERAL_LISTENPORT=7050
          - ORDERER_GENERAL_LEDGERTYPE=ram
          - ORDERER_GENERAL_GENESISMETHOD=file
          - ORDERER_GENERAL_GENESISFILE=/etc/hyperledger/configtx/genesis.block
          - CONFIGTX_ORDERER_ORDERERTYPE=solo
          - CONFIGTX_ORDERER_BATCHSIZE_MAXMESSAGECOUNT=${CONFIGTX_ORDERER_BATCHSIZE_MAXMESSAGECOUNT}
          - CONFIGTX_ORDERER_BATCHTIMEOUT=${CONFIGTX_ORDERER_BATCHTIMEOUT}
          - CONFIGTX_ORDERER_ADDRESSES=[127.0.0.1:7050]
      volumes:
          - ../network-config/:/var/hyperledger/configs
          - ../crypto-config/ordererOrganizations/kdtrace.vn/users:/var/hyperledger/users
      working_dir: /opt/gopath/src/github.com/hyperledger/fabric/orderer
      command: orderer
      ports:
        - '7050'

  # orderer:
  #   image: hyperledger/fabric-orderer:1.4.0
  #   environment:
  #     - GODEBUG=netdns=go
  #     - FABRIC_LOGGING_SPEC=info
  #     - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
  #     - ORDERER_GENERAL_GENESISMETHOD=file
  #     - ORDERER_GENERAL_GENESISFILE=/etc/hyperledger/configtx/genesis.block
  #     - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
  #     - ORDERER_GENERAL_LOCALMSPDIR=/etc/hyperledger/msp/orderer/msp
  #     # kafka
  #     - ORDERER_KAFKA_RETRY_SHORTINTERVAL=1s
  #     - ORDERER_KAFKA_RETRY_SHORTTOTAL=30s
  #     - ORDERER_KAFKA_VERBOSE=true
  #   working_dir: /opt/gopath/src/github.com/hyperledger/fabric/orderer
  #   command: orderer
  # kafka:
  #   image: hyperledger/fabric-kafka
  #   restart: always
  #   environment:
  #     # ========================================================================
  #     #     Reference: https://kafka.apache.org/documentation/#configuration
  #     # ========================================================================
  #     #
  #     # socket.request.max.bytes
  #     # The maximum number of bytes in a socket request. ATTN: If you set this
  #     # env var, you should make sure that the value assigned to
  #     # `brokerConfig.Producer.MaxMessageBytes` in `newBrokerConfig()` in
  #     # `fabric/orderer/kafka/util.go` matches it.
  #     #- KAFKA_SOCKET_REQUEST_MAX_BYTES=104857600 # 100 * 1024 * 1024 B
  #     #
  #     # message.max.bytes
  #     # The maximum size of envelope that the broker can receive.
  #     - KAFKA_MESSAGE_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
  #     #
  #     # replica.fetch.max.bytes
  #     # The number of bytes of messages to attempt to fetch for each channel.
  #     # This is not an absolute maximum, if the fetched envelope is larger than
  #     # this value, the envelope will still be returned to ensure that progress
  #     # can be made. The maximum message size accepted by the broker is defined
  #     # via message.max.bytes above.
  #     - KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 # 99 * 1024 * 1024 B
  #     #
  #     # unclean.leader.election.enable
  #     # Data consistency is key in a blockchain environment. We cannot have a
  #     # leader chosen outside of the in-sync replica set, or we run the risk of
  #     # overwriting the offsets that the previous leader produced, and --as a
  #     # result-- rewriting the blockchain that the orderers produce.
  #     - KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false

  #     #
  #     # min.insync.replicas
  #     # Let the value of this setting be M. Data is considered committed when
  #     # it is written to at least M replicas (which are then considered in-sync
  #     # and belong to the in-sync replica set, or ISR). In any other case, the
  #     # write operation returns an error. Then:
  #     # 1. if just one replica out of the N (see default.replication.factor
  #     # below) that the channel data is written to becomes unavailable,
  #     # operations proceed normally.
  #     # 2. If N - M + 1 (or more) replicas become unavailable, Kafka cannot
  #     # maintain an ISR set of M, so it stops accepting writes. Reads work
  #     # without issues. The cluster becomes writeable again when M replicas get
  #     # in-sync.
  #     - KAFKA_MIN_INSYNC_REPLICAS=1
  #     #
  #     # default.replication.factor
  #     # Let the value of this setting be M. This means that:
  #     # 1. Each channel will have its data replicated to N brokers. These are
  #     # the candidates for the ISR set for a channel. As we've noted in the
  #     # min.insync.replicas section above, not all of these brokers have to be
  #     # available all the time. We choose a default.replication.factor of N so
  #     # as to have the largest possible candidate set for a channel's ISR.
  #     # 2. Channel creations cannot go forward if less than N brokers are up.
  #     - KAFKA_DEFAULT_REPLICATION_FACTOR=2
  #     #
  #     # zookeper.connect
  #     # Point to the set of Zookeeper nodes comprising a ZK ensemble.
  #     - KAFKA_ZOOKEEPER_CONNECT=zookeeper1:2181,zookeeper2:2181,zookeeper3:2181
  #     #
  #     # zookeeper.connection.timeout.ms
  #     # The max time that the client waits to establish a connection to
  #     # Zookeeper. If not set, the value in zookeeper.session.timeout.ms (below)
  #     # is used.
  #     #- KAFKA_ZOOKEEPER_CONNECTION_TIMEOUT_MS = 6000
  #     #
  #     # zookeeper.session.timeout.ms
  #     #- KAFKA_ZOOKEEPER_SESSION_TIMEOUT_MS = 6000
  #     #ports:
  #     #- '9092'