package org.hyperledger.fabric.chaincode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.io.IOException;
import java.util.Objects;

@DataType()
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class DeliveryTruck {
    private Long id;

    private String numberPlate;

    private String autoMaker;

    private String status;

    private String transportId;

    private String create_at;

    @Override
    public int hashCode() {
        return Objects.hash(id, numberPlate, autoMaker, status, transportId, create_at);
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", numberPlate='" + numberPlate + "'" +
                ", autoMaker='" + autoMaker + "'" +
                ", status='" + status + "'" +
                ", transportId='" + transportId + "'" +
                ", create_at='" + create_at + "'" +
                "}";
    }

    public String toJSONString() throws IOException {
        return new ObjectMapper().writeValueAsString(this);
    }
}
