package org.hyperledger.fabric.chaincode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.io.IOException;
import java.util.Objects;
@DataType()
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Producer {
    private Long userId;

    private String username;

    private String role;

    private Long producerId;

    private String companyName;

    private String email;

    private String address;

    private String phone;

    private String avatar;

    private String website;

    private String tin;

    private String create_at;

    private String update_at;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Producer)) {
            return false;
        }
        Producer producer = (Producer) o;
        return Objects.equals(userId, producer.userId)
                && Objects.equals(username, producer.username)
                && Objects.equals(role, producer.role)
                && Objects.equals(producerId, producer.producerId)
                && Objects.equals(companyName, producer.companyName)
                && Objects.equals(email, producer.email)
                && Objects.equals(address, producer.address)
                && Objects.equals(phone, producer.phone)
                && Objects.equals(avatar, producer.avatar)
                && Objects.equals(create_at, producer.create_at)
                && Objects.equals(update_at, producer.update_at)
                && Objects.equals(website, producer.website)
                && Objects.equals(tin, producer.tin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username, role, producerId, companyName, email, address, phone, avatar, website, tin, create_at, update_at);
    }

    @Override
    public String toString() {
        return "{" +
                " userId='" + getUserId() + "'" +
                ", username='" + getUsername() + "'" +
                ", role='" + getRole() + "'" +
                ", producerId='" + getProducerId() + "'" +
                ", companyName='" + getCompanyName() + "'" +
                ", email='" + getEmail() + "'" +
                ", address='" + getAddress() + "'" +
                ", phone='" + getPhone() + "'" +
                ", avatar='" + getAvatar() + "'" +
                ", website='" + getWebsite() + "'" +
                ", tin='" + getTin() + "'" +
                ", create_at='" + getCreate_at() + "'" +
                ", update_at='" + getUpdate_at() + "'" +
                "}";
    }

    public String toJSONString() throws IOException {
        return new ObjectMapper().writeValueAsString(this);
    }
}
