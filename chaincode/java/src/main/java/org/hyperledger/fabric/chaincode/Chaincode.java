package org.hyperledger.fabric.chaincode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.chaincode.accessControl.ProcessAccessRule;
import org.hyperledger.fabric.chaincode.accessControl.ProductAccessRule;
import org.hyperledger.fabric.chaincode.accessControl.UserAccessRule;
import org.hyperledger.fabric.chaincode.process.LedgerProcess;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.*;
import org.hyperledger.fabric.shim.ChaincodeBase;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.util.Arrays;
import java.util.List;

@Contract(
        name = "KDTrace",
        info = @Info(
                title = "KDTrace contract",
                description = "The hyperlegendary process contract",
                version = "0.0.1-SNAPSHOT",
                license = @License(
                        name = "Apache 2.0 License",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"),
                contact = @Contact(
                        email = "trungkien98.77@gmail.com",
                        name = "KDTrace",
                        url = "http://www.kdtrace.xyz")))
@Default
public class Chaincode extends ChaincodeBase implements ContractInterface {

    private static Log _logger = LogFactory.getLog(Chaincode.class);
    private static ProcessAccessRule processAccessRule = ProcessAccessRule.createNewInstance();
    private static LedgerProcess ledgerProcess = LedgerProcess.createNewInstance();
    private static UserAccessRule userAccessRule = UserAccessRule.createNewInstance();
    private static ProductAccessRule productAccessRule = ProductAccessRule.createNewInstance();

    @Override
    public Response invoke(ChaincodeStub stub) {
        try {
            _logger.info("Invoke Chaincode ");
            String func = stub.getFunction();
            List<String> params = stub.getParameters();
            _logger.info("function: " + func +", param: "+params.toString());

            Response response;
            switch (func) {
                case "query":
                    response = query(stub, params);
                    break;
                case "updateProducer":
                    response = updateProducer(stub, params);
                    break;
                case "updateTransport":
                    response = updateTransport(stub, params);
                    break;
                case "updateDeliveryTruck":
                    response = updateDeliveryTruck(stub, params);
                    break;
                case "updateDistributor":
                    response = updateDistributor(stub, params);
                    break;
                case "updateProduct":
                    response = updateProduct(stub, params);
                    break;
                case "createQRCodes":
                    response = createQRCodes(stub, params);
                    break;
                case "saveQRCodes":
                    response = saveQRCodes(stub, params);
                    break;
                case "createProcess":
                    response = createProcess(stub, params);
                    break;
                case "updateProcess":
                    response = updateProcess(stub, params);
                    break;
                case "getProcesses":
                    response = getProcesses(stub, params);
                    break;
                default:
                    throw new RuntimeException("Invalid invoke function name [" + func + "]!");
            }
            return response;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Response init(ChaincodeStub stub) {
        _logger.info("Init Chaincode");
        return ledgerProcess.init(stub);
    }

    private Response query(ChaincodeStub stub, List<String> args) {
        _logger.info("query with args: " + Arrays.toString(args.toArray()));
        return ledgerProcess.query(stub, args);
    }

    private Response updateProducer(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke updateProducer with args: " + Arrays.toString(args.toArray()));
        return userAccessRule.validateUpdateProducer(stub, args);
    }
    private Response updateTransport(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke updateTransport with args: " + Arrays.toString(args.toArray()));
        return userAccessRule.validateUpdateTransport(stub, args);
    }
    private Response updateDeliveryTruck(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke updateDeliveryTruck with args: " + Arrays.toString(args.toArray()));
        return userAccessRule.validateUpdateDeliveryTruck(stub, args);
    }
    private Response updateDistributor(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke updateDistributor with args: " + Arrays.toString(args.toArray()));
        return userAccessRule.validateUpdateDistributor(stub, args);
    }
    private Response updateProduct(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke updateProduct with args: " + Arrays.toString(args.toArray()));
        return productAccessRule.validateUpdateProduct(stub, args);
    }
    private Response createQRCodes(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke createQRCodes with args: " + Arrays.toString(args.toArray()));
        return productAccessRule.validateCreateQRCodes(stub, args);
    }
    private Response saveQRCodes(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke saveQRCodes with args: " + Arrays.toString(args.toArray()));
        return productAccessRule.validateSaveQRCodes(stub, args);
    }
    private Response createProcess(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke saveProcess with args: " + Arrays.toString(args.toArray()));
        return processAccessRule.validateCreateProcess(stub, args);
    }
    private Response updateProcess(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke updateProcess with args: " + Arrays.toString(args.toArray()));
        return processAccessRule.validateUpdateProcess(stub, args);
    }

    private Response getProcesses(ChaincodeStub stub, List<String> args) throws Exception {
        _logger.info("Invoke test with args: " + Arrays.toString(args.toArray()));
        return processAccessRule.getProcesses(stub, args);
    }

    public static void main(String[] args) {
        new Chaincode().start(args);
    }

}
