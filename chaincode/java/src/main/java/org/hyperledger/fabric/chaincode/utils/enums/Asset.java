package org.hyperledger.fabric.chaincode.utils.enums;

import java.util.Arrays;
import java.util.List;

public enum Asset {
    PROCESS("PROCESS");

    private String code;

    Asset(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
