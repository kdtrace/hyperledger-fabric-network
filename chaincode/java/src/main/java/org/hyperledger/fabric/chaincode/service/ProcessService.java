package org.hyperledger.fabric.chaincode.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.chaincode.model.Process;
import org.hyperledger.fabric.shim.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.hyperledger.fabric.shim.ResponseUtils.newSuccessResponse;

public class ProcessService {
    private static Log _logger = LogFactory.getLog(ProcessService.class);

    public static ProcessService createNewInstance() {
        return new ProcessService();
    }


    public Chaincode.Response saveProcess(ChaincodeStub stub, Process process){
        try {
            String key = "PROCESS-" + process.getId();
            _logger.info("Save process: " + key);
            byte[] newData = process.toJSONString().getBytes(StandardCharsets.UTF_8);
            stub.putState(key, newData);
            return newSuccessResponse("Save process successfully");
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    //Chuwa xong nha
    public Chaincode.Response getProcesses(ChaincodeStub stub, List<String> args ) {
        try {
            String queryString = "{\"selector\": {}, \"fields\":[\"_id\", \"_rev\", \"key\", \"id\", \"projectId\", \"projectName\", \"assigneeId\", \"assigneeName\", \"status\", \"title\", \"description\", \"startDate\", \"dueDate\", \"estimatedHours\", \"realStartDate\", \"realDueDate\", \"realHours\", \"updateBy\"]}";
            QueryResultsIterator<KeyValue> queryResults = stub.getQueryResult(queryString);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            boolean isHasElementBefore = false;
            for (KeyValue result : queryResults) {
                if (isHasElementBefore) {
                    stringBuilder.append(",");
                }
                stringBuilder.append(new String(result.getValue(), StandardCharsets.UTF_8));
                if (!isHasElementBefore) {
                    isHasElementBefore = true;
                }
            }
            stringBuilder.append("]");
            String results = stringBuilder.toString();
            _logger.info(results);
            return newSuccessResponse(results);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
