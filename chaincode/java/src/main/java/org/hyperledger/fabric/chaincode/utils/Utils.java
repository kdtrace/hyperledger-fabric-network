package org.hyperledger.fabric.chaincode.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.chaincode.model.Process;
import org.hyperledger.fabric.chaincode.utils.enums.Asset;


public class Utils {

    private static Log _logger = LogFactory.getLog(Utils.class);

    public static String createProcessIdFromId(String id) {
        return Asset.PROCESS.getCode() + "-" + id;
    }
    public static String createProcessIdFromProcess(Process process) {
        return Asset.PROCESS.getCode() + "-" + process.getId();
    }

}
