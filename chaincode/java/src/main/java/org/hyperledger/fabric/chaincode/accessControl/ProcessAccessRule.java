package org.hyperledger.fabric.chaincode.accessControl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hyperledger.fabric.chaincode.model.DeliveryTruck;
import org.hyperledger.fabric.chaincode.model.Process;
import org.hyperledger.fabric.chaincode.model.QRCode;
import org.hyperledger.fabric.chaincode.service.ProcessService;
import org.hyperledger.fabric.chaincode.service.ProductService;
import org.hyperledger.fabric.chaincode.service.UserService;
import org.hyperledger.fabric.shim.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hyperledger.fabric.shim.ResponseUtils.newSuccessResponse;

public class ProcessAccessRule {
    private static Log _logger = LogFactory.getLog(ProcessAccessRule.class);

    private static ProcessService processService = ProcessService.createNewInstance();
    private static ProductService productService = ProductService.createNewInstance();
    private static UserService userService = UserService.createNewInstance();

    public static ProcessAccessRule createNewInstance() {
        return new ProcessAccessRule();
    }

    public Chaincode.Response validateCreateProcess(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        _logger.info("Map process object");
        Process process = mapper.readValue(args.get(0), Process.class);
        _logger.info("Process object: " + process.toString());

        return processService.saveProcess(stub, process);
    }

    public Chaincode.Response validateUpdateProcess(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Long processId = mapper.readValue(args.get(0), Long.class);
        String statusProcess = mapper.readValue(args.get(1), String.class);
        List<Long> qrCodes = new ArrayList<>();
        Long transportId = null;
        Long deliveryTruckId = null;
        DeliveryTruck deliveryTruck = null;
        String delivery_at = null;
        String receipt_at = null;
        if (!args.get(3).isEmpty()){
            transportId = mapper.readValue(args.get(3), Long.class);
        }
        if (!args.get(4).isEmpty()){
            deliveryTruckId = mapper.readValue(args.get(4), Long.class);
            deliveryTruck = mapper.readValue(stub.getStringState("DELIVERY_TRUCK-" + deliveryTruckId), DeliveryTruck.class);
        }
        if (!args.get(5).isEmpty()){
            delivery_at = mapper.readValue(args.get(5), String.class);
        }
        if (!args.get(6).isEmpty()){
            receipt_at = mapper.readValue(args.get(6), String.class);
        }
        if (!args.get(2).isEmpty()){
            mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            qrCodes = mapper.readValue(args.get(2), new TypeReference<List<Long>>(){});
        }
        _logger.info("update process: " + processId + ", statusProcess: " + statusProcess);

        Process process = mapper.readValue(stub.getStringState("PROCESS-" + processId), Process.class);
        List<QRCode> qrCodeList = new ArrayList<>();
        qrCodes.forEach(id -> {
            try {
                QRCode qrCode = mapper.readValue(stub.getStringState("QRCODE-" + id), QRCode.class);
                qrCode.setProcessId(process.getId());
                qrCodeList.add(qrCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        switch (statusProcess) {
            case "PRODUCER_REJECT":
                process.setStatusProcess(statusProcess);
                processService.saveProcess(stub, process);
                break;
            case "CHOOSE_DELIVERYTRUCK_TRANSPORT":
                process.setStatusProcess(statusProcess);
                process.setQrCodes(qrCodes);
                processService.saveProcess(stub, process);
                productService.saveQRCodes(stub, qrCodeList);
                break;
            case "WAITING_RESPONSE_TRANSPORT":
                process.setStatusProcess(statusProcess);
                process.setTransportId(transportId);
                processService.saveProcess(stub, process);
                break;
            case "ON_BOARDING_GET":
                process.setStatusProcess(statusProcess);
                process.setDeliveryTruckId(deliveryTruckId);
                processService.saveProcess(stub, process);
                deliveryTruck.setStatus("ON_DELIVERY");
                userService.updateDeliveryTruck(stub, deliveryTruck);
                break;
            case "ON_BOARDING_RECEIVE":
                process.setStatusProcess(statusProcess);
                process.setDelivery_at(delivery_at);
                processService.saveProcess(stub, process);
                break;
            case "RECEIVED":
                process.setStatusProcess(statusProcess);
                process.setReceipt_at(receipt_at);
                processService.saveProcess(stub, process);
                break;
        }
        return newSuccessResponse("Update Process and QRCodes successfully");
    }

    public Chaincode.Response getProcesses(ChaincodeStub stub, List<String> args) throws Exception {
        return processService.getProcesses(stub, args);
    }
}
