package org.hyperledger.fabric.chaincode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.io.IOException;
import java.util.Objects;

@DataType()
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class QRCode {
    private Long id;

    private Long productId;

    private Long processId;

    private String code;

    private String ower;

    private String link;

    private String statusQRCode;

    private String create_at;

    private String otp;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof QRCode)) {
            return false;
        }
        QRCode qrCode = (QRCode) o;
        return Objects.equals(id, qrCode.id) && Objects.equals(productId, qrCode.productId) && Objects.equals(processId, qrCode.processId)  && Objects.equals(code, qrCode.code) &&
                Objects.equals(ower, qrCode.ower) && Objects.equals(link, qrCode.link) && Objects.equals(statusQRCode, qrCode.statusQRCode) && Objects.equals(create_at, qrCode.create_at) && Objects.equals(otp, qrCode.otp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productId, processId, code, ower, link, statusQRCode, create_at, otp);
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", productId='" + getProductId() + "'" +
                ", processId='" + getProcessId() + "'" +
                ", code='" + getCode() + "'" +
                ", ower='" + getOwer() + "'" +
                ", link='" + getLink() + "'" +
                ", statusQRCode='" + getStatusQRCode() + "'" +
                ", create_at='" + getCreate_at() + "'" +
                ", otp='" + getOtp() + "'" +
                "}";
    }

    public String toJSONString() throws IOException {
        return new ObjectMapper().writeValueAsString(this);
    }
}

