package org.hyperledger.fabric.chaincode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@DataType()
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Product {
    private Long id;

    private String name;

    private String type;

    private Date mfg;

    private Date exp;

    private List<Long> codes;

    private String producerId;

    private String userId;

    private long quantity;

    private String unit;

    private String create_at;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Product)) {
            return false;
        }
        Product product = (Product) o;
        return Objects.equals(id, product.id) && Objects.equals(name, product.name) && Objects.equals(type, product.type) && Objects.equals(mfg, product.mfg) && Objects.equals(exp, product.exp)
                && Objects.equals(codes, product.codes) && Objects.equals(producerId, product.producerId) && Objects.equals(userId, product.userId) && Objects.equals(quantity, product.quantity) && Objects.equals(unit, product.unit) && Objects.equals(create_at, product.create_at);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, mfg, exp, codes, producerId, userId, quantity, unit, create_at);
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", name='" + getName() + "'" +
                ", type='" + getType() + "'" +
                ", dateOfExpiration='" + getMfg() + "'" +
                ", exp='" + getExp() + "'" +
                ", codes='" + getCodes() + "'" +
                ", producerId='" + getProducerId() + "'" +
                ", userId='" + getUserId() + "'" +
                ", quantity='" + getQuantity() + "'" +
                ", unit='" + getUnit() + "'" +
                ", create_at='" + getCreate_at() + "'" +
                "}";
    }

    public String toJSONString() throws IOException {
        return new ObjectMapper().writeValueAsString(this);
    }
}
