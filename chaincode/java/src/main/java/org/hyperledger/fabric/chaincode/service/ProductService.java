package org.hyperledger.fabric.chaincode.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.chaincode.model.Product;
import org.hyperledger.fabric.chaincode.model.QRCode;
import org.hyperledger.fabric.shim.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.hyperledger.fabric.shim.ResponseUtils.newSuccessResponse;

public class ProductService {
    private static Log _logger = LogFactory.getLog(ProductService.class);

    public static ProductService createNewInstance() {
        return new ProductService();
    }

    public Chaincode.Response updateProduct(ChaincodeStub stub, Product product) {
        try {
            String key = "PRODUCT-" + product.getId();
            _logger.info("Update producer: " + key);
            byte[] newData = product.toJSONString().getBytes(StandardCharsets.UTF_8);
            stub.putState(key, newData);
            return newSuccessResponse("Update producer successfully");
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Chaincode.Response saveQRCodes(ChaincodeStub stub, List<QRCode> qrCodes) {
        if (qrCodes.size() == 0)
            throw new RuntimeException("QRCode list is null");
        else
            qrCodes.forEach(qrCode -> {
                try {
                    String key = "QRCODE-" + qrCode.getId();
                    _logger.info("save QRCode: " + key);
                    byte[] newData = qrCode.toJSONString().getBytes(StandardCharsets.UTF_8);
                    stub.putState(key, newData);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            });
        return newSuccessResponse("Save QRCodes successfully");
    }
}
