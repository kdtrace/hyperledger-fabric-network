package org.hyperledger.fabric.chaincode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.io.IOException;
import java.util.Objects;

@DataType()
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Transport {
    private Long userId;

    private String username;

    private String role;

    private Long transportId;

    private String companyName;

    private String email;

    private String address;

    private String phone;

    private String avatar;

    private String website;

    private String tin;

    private String create_at;

    private String update_at;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Transport)) {
            return false;
        }
        Transport transport = (Transport) o;
        return Objects.equals(userId, transport.userId)
                && Objects.equals(username, transport.username)
                && Objects.equals(role, transport.role)
                && Objects.equals(transportId, transport.transportId)
                && Objects.equals(companyName, transport.companyName)
                && Objects.equals(email, transport.email)
                && Objects.equals(address, transport.address)
                && Objects.equals(phone, transport.phone)
                && Objects.equals(avatar, transport.avatar)
                && Objects.equals(create_at, transport.create_at)
                && Objects.equals(update_at, transport.update_at)
                && Objects.equals(website, transport.website)
                && Objects.equals(tin, transport.tin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username, role, transportId, companyName, email, address, phone, avatar, create_at, update_at);
    }

    @Override
    public String toString() {
        return "{" +
                " userId='" + getUserId() + "'" +
                ", username='" + getUsername() + "'" +
                ", role='" + getRole() + "'" +
                ", transportId='" + getTransportId() + "'" +
                ", companyName='" + getCompanyName() + "'" +
                ", email='" + getEmail() + "'" +
                ", address='" + getAddress() + "'" +
                ", phone='" + getPhone() + "'" +
                ", avatar='" + getAvatar() + "'" +
                ", website='" + getWebsite() + "'" +
                ", tin='" + getTin() + "'" +
                ", create_at='" + getCreate_at() + "'" +
                ", update_at='" + getUpdate_at() + "'" +
                "}";
    }

    public String toJSONString() throws IOException {
        return new ObjectMapper().writeValueAsString(this);
    }
}
