package org.hyperledger.fabric.chaincode.accessControl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.chaincode.model.DeliveryTruck;
import org.hyperledger.fabric.chaincode.model.Distributor;
import org.hyperledger.fabric.chaincode.model.Producer;
import org.hyperledger.fabric.chaincode.model.Transport;
import org.hyperledger.fabric.chaincode.service.UserService;
import org.hyperledger.fabric.contract.ClientIdentity;
import org.hyperledger.fabric.shim.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.util.List;

public class UserAccessRule {
    private static Log _logger = LogFactory.getLog(UserAccessRule.class);

    private static UserService userService = UserService.createNewInstance();

    public static UserAccessRule createNewInstance(){
        return new UserAccessRule();
    }

    public Chaincode.Response validateUpdateProducer(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ClientIdentity clientIdentity = new ClientIdentity(stub);
        String userId = clientIdentity.getAttributeValue("id");
        Producer producer = mapper.readValue(args.get(0), Producer.class);
        _logger.info("Producer Info object: " + producer.toString());
        return userService.updateProducer(stub, userId, producer);
    }
    public Chaincode.Response validateUpdateTransport(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ClientIdentity clientIdentity = new ClientIdentity(stub);
        String userId = clientIdentity.getAttributeValue("id");
        Transport transport = mapper.readValue(args.get(0), Transport.class);
        _logger.info("Transport Info object: " + transport.toString());
        return userService.updateTransport(stub, userId, transport);
    }
    public Chaincode.Response validateUpdateDeliveryTruck(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DeliveryTruck deliveryTruck = mapper.readValue(args.get(0), DeliveryTruck.class);
        _logger.info("DeliveryTruck Info object: " + deliveryTruck.toString());
        return userService.updateDeliveryTruck(stub, deliveryTruck);
    }
    public Chaincode.Response validateUpdateDistributor(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ClientIdentity clientIdentity = new ClientIdentity(stub);
        String userId = clientIdentity.getAttributeValue("id");
        Distributor distributor = mapper.readValue(args.get(0), Distributor.class);
        _logger.info("Distributor Info object: " + distributor.toString());
        return userService.updateDistributor(stub, userId, distributor);
    }
}
