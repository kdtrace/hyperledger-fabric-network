package org.hyperledger.fabric.chaincode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.io.IOException;
import java.util.Objects;

@DataType()
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Distributor {
    private Long userId;

    private String username;

    private String role;

    private Long distributorId;

    private String companyName;

    private String email;

    private String address;

    private String phone;

    private String avatar;

    private String website;

    private String tin;

    private String create_at;

    private String update_at;

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Distributor)) {
            return false;
        }
        Distributor distributor = (Distributor) o;
        return Objects.equals(userId, distributor.userId)
                && Objects.equals(username, distributor.username)
                && Objects.equals(role, distributor.role)
                && Objects.equals(distributorId, distributor.distributorId)
                && Objects.equals(companyName, distributor.companyName)
                && Objects.equals(email, distributor.email)
                && Objects.equals(address, distributor.address)
                && Objects.equals(phone, distributor.phone)
                && Objects.equals(avatar, distributor.avatar)
                && Objects.equals(create_at, distributor.create_at)
                && Objects.equals(update_at, distributor.update_at)
                && Objects.equals(website, distributor.website)
                && Objects.equals(tin, distributor.tin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username, role, distributorId, companyName, email, address, phone, avatar, create_at, update_at);
    }

    @Override
    public String toString() {
        return "{" +
                " userId='" + getUserId() + "'" +
                ", username='" + getUsername() + "'" +
                ", role='" + getRole() + "'" +
                ", producerId='" + getDistributorId() + "'" +
                ", companyName='" + getCompanyName() + "'" +
                ", email='" + getEmail() + "'" +
                ", address='" + getAddress() + "'" +
                ", phone='" + getPhone() + "'" +
                ", avatar='" + getAvatar() + "'" +
                ", website='" + getWebsite() + "'" +
                ", tin='" + getTin() + "'" +
                ", create_at='" + getCreate_at() + "'" +
                ", update_at='" + getUpdate_at() + "'" +
                "}";
    }

    public String toJSONString() throws IOException {
        return new ObjectMapper().writeValueAsString(this);
    }
}
