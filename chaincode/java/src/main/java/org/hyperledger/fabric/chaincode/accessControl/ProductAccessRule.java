package org.hyperledger.fabric.chaincode.accessControl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hyperledger.fabric.chaincode.model.Product;
import org.hyperledger.fabric.chaincode.model.QRCode;
import org.hyperledger.fabric.chaincode.service.ProductService;
import org.hyperledger.fabric.contract.ClientIdentity;
import org.hyperledger.fabric.shim.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductAccessRule {
    private static Log _logger = LogFactory.getLog(ProductAccessRule.class);

    private static ProductService productService = ProductService.createNewInstance();

    public static ProductAccessRule createNewInstance() {
        return new ProductAccessRule();
    }

    public Chaincode.Response validateUpdateProduct(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ClientIdentity clientIdentity = new ClientIdentity(stub);
        String userId = clientIdentity.getAttributeValue("id");
        Product product = mapper.readValue(args.get(0), Product.class);
        if (!product.getUserId().equals(userId))
            throw new RuntimeException("Don't have permission");
        _logger.info("Product Info object: " + product.toString());
        return productService.updateProduct(stub, product);
    }

    public Chaincode.Response validateCreateQRCodes(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        List<QRCode> qrCodes = mapper.readValue(args.get(0), new TypeReference<List<QRCode>>() {});
        return productService.saveQRCodes(stub, qrCodes);
    }

    public Chaincode.Response validateSaveQRCodes(ChaincodeStub stub, List<String> args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        List<QRCode> qrCodeList = new ArrayList<>();
        List<Long> qrCodes = new ArrayList<>();
        Map<Long, String> mapOtp = null;
        String statusQRCode = mapper.readValue(args.get(1), String.class);
        //save
        if (!args.get(0).isEmpty()) {
            mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            qrCodes = mapper.readValue(args.get(0), new TypeReference<List<Long>>() {});
            qrCodes.forEach(id -> {
                try {
                    QRCode qrCode = mapper.readValue(stub.getStringState("QRCODE-" + id), QRCode.class);
                    qrCode.setStatusQRCode(statusQRCode);
                    qrCodeList.add(qrCode);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        //save with otp
        else if (!args.get(2).isEmpty()) {
            mapOtp = mapper.readValue(args.get(2), new TypeReference<HashMap<Long, String>>() {});
            mapOtp.forEach((id, otp) -> {
                try {
                    QRCode qrCode = mapper.readValue(stub.getStringState("QRCODE-" + id), QRCode.class);
                    qrCode.setStatusQRCode(statusQRCode);
                    qrCode.setOtp(otp);
                    qrCodeList.add(qrCode);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        _logger.info("saveQRCodes: " + qrCodes.toString() + ", statusQRCode: " + statusQRCode);
        return productService.saveQRCodes(stub, qrCodeList);
    }

}
