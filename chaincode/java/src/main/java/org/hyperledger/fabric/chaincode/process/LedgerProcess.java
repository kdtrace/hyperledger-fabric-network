package org.hyperledger.fabric.chaincode.process;

import com.google.protobuf.ByteString;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.Chaincode.*;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ResponseUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class LedgerProcess {

    private static Log _logger = LogFactory.getLog(LedgerProcess.class);

    public static LedgerProcess createNewInstance() {
        return new LedgerProcess();
    }

    @Transaction
    public Response init(ChaincodeStub stub) {
        try {
            String func = stub.getFunction();
            if (!func.equals("init")) {
                throw new RuntimeException("function other than init is not supported");
            }
//            List<String> args = stub.getParameters();
//            if (args.size() != 4) {
//                throw new RuntimeException("Incorrect number of arguments. Expecting 4");
//            }
            String buffer = "{\"id\": 1,\"projectId\": 1,\"assigneeId\": 1,\"status\": \"PENDING\",\"title\": null," +
                    "\"description\": \"THIS IS A BIG PROBLEM. IT IS VERY DIFFICULT!\", " +
                    "\"startDate\": \"2019-06-13 10:09:21\",\"dueDate\": \"2019-06-13 10:09:21\"," +
                    "\"estimatedHours\": 8," +
                    "\"realStartDate\": \"2019-06-13 10:09:21\"," +
                    "\"realDueDate\": \"2019-06-13 10:09:21\"," +
                    "\"realHours\": 9," +
                    "\"updateBy\": 1," +
                    "\"emptyAny\": false}";
            stub.putState("a", buffer.getBytes(StandardCharsets.UTF_8));
            return ResponseUtils.newSuccessResponse();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @Transaction
    public Response query(ChaincodeStub stub, List<String> args) {
        if (args.size() != 1) {
            throw new RuntimeException("Incorrect number of arguments. Expecting name of the person to query");
        }
        _logger.info(String.format("query with key = %s",args.get(0)));
        String key = args.get(0);
        //byte[] stateBytes
        String val	= stub.getStringState(key);
        if (val == null) {
            throw new RuntimeException(String.format("Error: state for %s is null", key));
        }
        _logger.info(String.format("Query Response:\nName: %s, Amount: %s\n", key, val));
        return ResponseUtils.newSuccessResponse(val, ByteString.copyFrom(val, StandardCharsets.UTF_8).toByteArray());
    }
//
//    @Transaction
//    public Response test(ChaincodeStub stub, List<String> args) {
//        if (args.size() != 1) {
//            throw new RuntimeException("Incorrect number of arguments. Expecting name of the person to query");
//        }
//        _logger.info(String.format("query with key = %s",args.get(0)));
//        String key = args.get(0);
//        //byte[] stateBytes
//        String val	= stub.getStringState(key);
//        if (val == null) {
//            throw new RuntimeException(String.format("Error: state for %s is null", key));
//        }
//        _logger.info(String.format("Query Response:\nName: %s, Amount: %s\n", key, val));
//        return ResponseUtils.newSuccessResponse(val, ByteString.copyFrom(val, StandardCharsets.UTF_8).toByteArray());
//    }
}
