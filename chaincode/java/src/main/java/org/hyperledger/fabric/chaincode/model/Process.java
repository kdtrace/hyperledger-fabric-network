package org.hyperledger.fabric.chaincode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@DataType()
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Process {
    private Long id;

    private Long distributorId;

    private List<Long> qrCodes;

    private Long deliveryTruckId;

    private Long transportId;

    private String statusProcess;

    private String delivery_at;

    private String receipt_at;

    private String create_at;

    @Override
    public int hashCode() {
        return Objects.hash(id, distributorId, qrCodes, deliveryTruckId, transportId, statusProcess, delivery_at, receipt_at, create_at);
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", distributorId='" + getDistributorId() + "'" +
                ", qrCodes='" + getQrCodes() + "'" +
                ", deliveryTruckId='" + getDeliveryTruckId() + "'" +
                ", transportId='" + getTransportId() + "'" +
                ", statusProcess='" + getStatusProcess() + "'" +
                ", delivery_at='" + getDelivery_at() + "'" +
                ", receipt_at='" + getReceipt_at() + "'" +
                ", create_at='" + getCreate_at() + "'" +
                "}";
    }

    public String toJSONString() throws IOException {
        return new ObjectMapper().writeValueAsString(this);
    }
}


