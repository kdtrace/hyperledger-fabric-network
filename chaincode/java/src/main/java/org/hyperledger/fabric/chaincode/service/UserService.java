package org.hyperledger.fabric.chaincode.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.chaincode.model.DeliveryTruck;
import org.hyperledger.fabric.chaincode.model.Distributor;
import org.hyperledger.fabric.chaincode.model.Producer;
import org.hyperledger.fabric.chaincode.model.Transport;
import org.hyperledger.fabric.shim.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.nio.charset.StandardCharsets;

import static org.hyperledger.fabric.shim.ResponseUtils.newSuccessResponse;

public class UserService {
    private static Log _logger = LogFactory.getLog(UserService.class);

    public static UserService createNewInstance() {
        return new UserService();
    }

    public Chaincode.Response updateProducer(ChaincodeStub stub, String userId, Producer producer) {
        try {
            String key = "USER-" + userId;
            _logger.info("Update producer: " + key);
            byte[] newData = producer.toJSONString().getBytes(StandardCharsets.UTF_8);
            stub.putState(key, newData);
            return newSuccessResponse("Update producer successfully");
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    public Chaincode.Response updateTransport(ChaincodeStub stub, String userId, Transport transport) {
        try {
            String key = "USER-" + userId;
            _logger.info("Update transport: " + key);
            byte[] newData = transport.toJSONString().getBytes(StandardCharsets.UTF_8);
            stub.putState(key, newData);
            return newSuccessResponse("Update transport successfully");
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    public Chaincode.Response updateDeliveryTruck(ChaincodeStub stub, DeliveryTruck deliveryTruck) {
        try {
            String key = "DELIVERY_TRUCK-" + deliveryTruck.getId();
            _logger.info("update DeliveryTruck: " + key);
            byte[] newData = deliveryTruck.toJSONString().getBytes(StandardCharsets.UTF_8);
            stub.putState(key, newData);
            return newSuccessResponse("Update DeliveryTruck successfully");
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    public Chaincode.Response updateDistributor(ChaincodeStub stub, String userId, Distributor distributor) {
        try {
            String key = "USER-" + userId;
            _logger.info("Update distributor: " + key);
            byte[] newData = distributor.toJSONString().getBytes(StandardCharsets.UTF_8);
            stub.putState(key, newData);
            return newSuccessResponse("Update distributor successfully");
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
