#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

# Environment variable for channel kdtrace
CHANNEL_NAME=kdtrace
CHANNEL_NAME_BLOCK=$CHANNEL_NAME.block
CHANNEL_TX=$CHANNEL_NAME-channel.tx

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=30
export FABRIC_PROCESS_TIMEOUT=10

CC_SRC_PATH=/opt/gopath/src/github.com/java
CC_RUNTIME_LANGUAGE=java

setup() {
    export CA1_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/org1.kdtrace.vn/ca && ls *_sk)
    export CA2_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/org2.kdtrace.vn/ca && ls *_sk)
    export CA3_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/org3.kdtrace.vn/ca && ls *_sk)

    docker-compose -f docker-compose-dev.yml down
    docker-compose -f docker-compose-dev.yml up -d
    docker ps -a

    #echo ${FABRIC_START_TIMEOUT}
    sleep ${FABRIC_START_TIMEOUT}

    echo "##################################################";
    echo "Create channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec \
        -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.kdtrace.vn/msp" \
        peer0.org1.kdtrace.vn peer channel create \
        -o orderer0.kdtrace.vn:7050 \
        -c $CHANNEL_NAME \
        -f /etc/hyperledger/configtx/$CHANNEL_TX

    sleep ${FABRIC_PROCESS_TIMEOUT}


    echo "##################################################";
    echo "Join peer0.org1.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.kdtrace.vn/msp" \
        peer0.org1.kdtrace.vn peer channel join \
        -b $CHANNEL_NAME_BLOCK

    echo "##################################################";
    echo "Join peer1.org1.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec peer1.org1.kdtrace.vn peer channel fetch newest $CHANNEL_NAME_BLOCK \
        -c $CHANNEL_NAME \
        --orderer orderer0.kdtrace.vn:7050
    docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.kdtrace.vn/msp" \
        peer1.org1.kdtrace.vn peer channel join \
        -b $CHANNEL_NAME_BLOCK

    echo "##################################################";
    echo "Join peer0.org2.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec peer0.org2.kdtrace.vn peer channel fetch newest $CHANNEL_NAME_BLOCK \
        -c $CHANNEL_NAME \
        --orderer orderer0.kdtrace.vn:7050
        docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org2.kdtrace.vn/msp" \
            peer0.org2.kdtrace.vn peer channel join \
            -b $CHANNEL_NAME_BLOCK

    echo "##################################################";
    echo "Join peer1.org2.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec peer1.org2.kdtrace.vn peer channel fetch newest $CHANNEL_NAME_BLOCK \
        -c $CHANNEL_NAME \
        --orderer orderer0.kdtrace.vn:7050
    docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org2.kdtrace.vn/msp" \
        peer1.org2.kdtrace.vn peer channel join \
        -b $CHANNEL_NAME_BLOCK

    echo "##################################################";
    echo "Join peer0.org3.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec peer0.org3.kdtrace.vn peer channel fetch newest $CHANNEL_NAME_BLOCK \
        -c $CHANNEL_NAME \
        --orderer orderer0.kdtrace.vn:7050
        docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org3.kdtrace.vn/msp" \
            peer0.org3.kdtrace.vn peer channel join \
            -b $CHANNEL_NAME_BLOCK

    echo "##################################################";
    echo "Join peer1.org3.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec peer1.org3.kdtrace.vn peer channel fetch newest $CHANNEL_NAME_BLOCK \
        -c $CHANNEL_NAME \
        --orderer orderer0.kdtrace.vn:7050
    docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org3.kdtrace.vn/msp" \
        peer1.org3.kdtrace.vn peer channel join \
        -b $CHANNEL_NAME_BLOCK

    echo "##################################################";
    echo "Update anchor peer peer0.org1.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.kdtrace.vn/msp" \
    peer0.org1.kdtrace.vn peer channel update --orderer orderer0.kdtrace.vn:7050 -c $CHANNEL_NAME -f /etc/hyperledger/configtx/Org1MSPanchors-kdtrace.tx 

    echo "##################################################";
    echo "Update anchor peer peer0.org2.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org2.kdtrace.vn/msp" \
    peer0.org2.kdtrace.vn peer channel update --orderer orderer0.kdtrace.vn:7050 -c $CHANNEL_NAME -f /etc/hyperledger/configtx/Org2MSPanchors-kdtrace.tx 

    echo "##################################################";
    echo "Update anchor peer peer0.org3.kdtrace.vn to the channel "$CHANNEL_NAME;
    echo "##################################################";
    docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org3.kdtrace.vn/msp" \
    peer0.org3.kdtrace.vn peer channel update --orderer orderer0.kdtrace.vn:7050 -c $CHANNEL_NAME -f /etc/hyperledger/configtx/Org3MSPanchors-kdtrace.tx 

   
    echo "##################################################";
    echo "Cli citynow install chaincode to peer0.org1.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.kdtrace.vn/users/Admin@org1.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        -v 1.0 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer1.org1.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.kdtrace.vn/users/Admin@org1.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer1.org1.kdtrace.vn:7054 \
        -v 1.0 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer0.org2.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.kdtrace.vn/users/Admin@org2.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer0.org2.kdtrace.vn:7055 \
        -v 1.0 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer1.org2.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.kdtrace.vn/users/Admin@org2.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer1.org2.kdtrace.vn:7056 \
        -v 1.0 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer0.org3.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org3MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.kdtrace.vn/users/Admin@org3.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer0.org3.kdtrace.vn:7061 \
        -v 1.0 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer1.org3.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org3MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.kdtrace.vn/users/Admin@org3.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer1.org3.kdtrace.vn:7064 \
        -v 1.0 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    

    echo "##################################################";
    echo "Instantiate chaincode";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.kdtrace.vn/users/Admin@org1.kdtrace.vn/msp" \
        cli peer chaincode instantiate \
        -o orderer0.kdtrace.vn:7050 \
        -C $CHANNEL_NAME \
        -n process_$CHANNEL_NAME \
        -l "$CC_RUNTIME_LANGUAGE" \
        -v 1.0 \
        -c '{"Args":["init","a","100","b","200"]}' \
        -P "OR ('Org1MSP.member','Org2MSP.member', 'Org3MSP.member')"

    sleep ${FABRIC_PROCESS_TIMEOUT}

    echo "##################################################";
    echo "Query Chaincode";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.kdtrace.vn/users/Admin@org1.kdtrace.vn/msp" \
        cli peer chaincode query \
        -C $CHANNEL_NAME \
        -n process_$CHANNEL_NAME \
        -c '{"Args":["query","a"]}'

        
    echo "##################################################";
    echo "That tuyet voi, he thong cua ban da hoat dong tot ";
    echo "#### Vao http://localhost:8090/#/ de truy cap ####";
    echo "################### Xin cam on ###################";
    echo "##################################################";
}

upgradeCC() {
    if [ $2 ]; then
        policy=$2
    else 
        policy="AND"
    fi

    echo "##################################################";
    echo "Cli citynow install chaincode to peer0.org1.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.kdtrace.vn/users/Admin@org1.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        -v $1 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer1.org1.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.kdtrace.vn/users/Admin@org1.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer1.org1.kdtrace.vn:7054 \
        -v $1 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer0.org2.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.kdtrace.vn/users/Admin@org2.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer0.org2.kdtrace.vn:7055 \
        -v $1 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer1.org2.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.kdtrace.vn/users/Admin@org2.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer1.org2.kdtrace.vn:7056 \
        -v $1 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer0.org3.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org3MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.kdtrace.vn/users/Admin@org3.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer0.org3.kdtrace.vn:7061 \
        -v $1 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "Cli citynow install chaincode to peer1.org3.kdtrace.vn";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org3MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.kdtrace.vn/users/Admin@org3.kdtrace.vn/msp" \
        cli peer chaincode install \
        -n process_$CHANNEL_NAME \
        --peerAddresses peer1.org3.kdtrace.vn:7064 \
        -v $1 \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

    echo "##################################################";
    echo "upgrade chaincode";
    echo "##################################################";
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
        -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.kdtrace.vn/users/Admin@org1.kdtrace.vn/msp" \
        cli peer chaincode upgrade \
        -o orderer0.kdtrace.vn:7050  \
        -C $CHANNEL_NAME \
        -n process_$CHANNEL_NAME \
        -l "$CC_RUNTIME_LANGUAGE" \
        -v $1 \
        -c '{"Args":["init","a","100","b","200"]}' \
        -P "$policy ('Org1MSP.member','Org2MSP.member', 'Org3MSP.member')"
}

while getopts "su" opt; do
  case "$opt" in
    s) setup
        exit 0;;
    u) upgradeCC ${@:$OPTIND:1} ${@:$OPTIND+1:1}
        exit 0;;
  esac
done
